from django.apps import AppConfig


class AppSdConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_SD'
